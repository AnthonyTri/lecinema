class Historique :
	def __init__(self) :
		self.commandes = []

	# commande est de type Commande
	def addCommande(self, commande) :
		self.commandes.append(commande)

	def showCommandes(self) :
		print("Liste des commandes :")
		for commande in self.commandes :
			print(commande)