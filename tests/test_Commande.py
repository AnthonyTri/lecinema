import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'Cinema'))
from Commande import Commande

def test_commandeTest() :
	# test si entree correspond aux attributs de la classe
	commandeTest = Commande(11, 1)
	assert isinstance(commandeTest, Commande)
	assert isinstance(commandeTest.prixPlace, int), "Input for prixPlace is not an integer"
	assert isinstance(commandeTest.nbPersonnes, int), "Input for nbPersonnes is not an integer"
