import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'Cinema'))
from Acteur import Acteur

def test_acteurTest() :
	# test si entree correspond aux attributs de la classe
	acteurTest = Acteur("John","Doe")
	assert isinstance(acteurTest, Acteur)
	assert isinstance(acteurTest.prenom, str), "John is not a string"
	assert isinstance(acteurTest.nom, str), "Doe is not a string"