FROM python:latest

COPY . /home/app

WORKDIR /home/app

RUN pip install pytest

CMD [ "python", "-m", "pytest" ]